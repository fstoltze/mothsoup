<?php if (get_field('display_page_title') != false) { ?>

<section id="moth-page-intro" class="intro-left">
  <div class="container">
    <h1 class="page-title <?php alignTitle(); ?>">
      <?php 
      if (get_field('custom_page_title')) {
        the_field('custom_page_title');
      } else {
        the_title();
      } 
      ?>
    </h1>
    <div class="page-subtitle <?php alignTitle(); ?>">
      <?php the_field('custom_page_subtitle'); ?>
    </div>

    <?php if (get_field('display_breadcrumbs') != false) {
      get_template_part( 'views/layout/breadcrumb' );
    } ?>

    <div class="clearboth">
      
    </div>
  </div>
</section>

<?php } // END IF DISLAY PAGE TITLE ?>

<?php if (get_field('stick_template') != true) {
  echo "<div class='stick-template-container'></div>";
} ?>