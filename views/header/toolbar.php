<div class="moth-header-toolbar">
  <div class="container header-grid">
    <div class="moth-toolbar-holder">

      <?php if (get_field('toolbar_tagline','option')) { ?>
        <?php get_template_part( 'views/header/toolbar/tagline' ); ?>
      <?php } ?>

      <?php if (get_field('toolbar_phone_number','option')) { ?>
        <?php get_template_part( 'views/header/toolbar/phone' ); ?>
      <?php } ?>

      <?php if (get_field('toolbar_email_address','option')) { ?>
        <?php get_template_part( 'views/header/toolbar/email' ); ?>
      <?php } ?>

      <?php if (get_field('header_social_networks_location','option') == "toolbar") {
        socialIcons(get_field('header_social_networks_style','option'),"light",get_field('header_social_networks_form','option'),"small");
      } ?>
    </div>
  </div>
</div>