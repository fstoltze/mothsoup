<nav class="navbar navbar-expand-md navbar-light bg-light">
   <div class="container">
   		<?php get_template_part( 'views/header/navigation/logo' ); ?>
   		<?php get_template_part( 'views/header/navigation/navigation' ); ?>
   		<?php if (get_field('search_form_in_header','option') != false) {get_search_form();} ?>
   		<?php if (get_field('header_social_networks_location','option') == "header") {
   		  socialIcons(get_field('header_social_networks_style','option'),"light",get_field('header_social_networks_form','option'),"large");
   		} ?>
   	</div>
</nav>