<a class="navbar-brand" href="<?php bloginfo( 'url' ); ?>">
<?php 
$dark_logo = get_field('dark_logo','option');
$light_logo = get_field('light_logo','option');

if( !empty($dark_logo) ): ?>
	<img src="<?php echo $dark_logo['url']; ?>" alt="<?php echo $dark_logo['alt']; ?>" class="d-inline-block align-top"/>
<?php else: ?>
	<img src="http://via.placeholder.com/350x60" width="350" height="60" class="d-inline-block align-top" alt="">
<?php endif; ?>
</a>