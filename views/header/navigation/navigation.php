  <?php
  wp_nav_menu([
    'menu'            => 'primary-menu',
    'theme_location'  => 'primary-menu',
    'container'       => 'div',
    'container_id'    => 'bs4navbar',
    'container_class' => 'ml-auto',
    'menu_id'         => false,
    'menu_class'      => 'navbar-nav ml-auto',
    'depth'           => 2,
    'fallback_cb'     => 'bs4navwalker::fallback',
    'walker'          => new bs4navwalker()
  ]);
  ?>