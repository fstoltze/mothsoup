<?php
/**
 * default search form
 */
?>
<form class="form-inline" role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-wrap">
        <input class="form-control" type="search" placeholder="<?php echo esc_attr( 'Search', 'mothsoup' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" aria-label="<?php echo esc_attr( 'Search', 'mothsoup' ); ?>" />
        <input class="btn btn-outline-success" type="submit" id="search-submit" value="Search" />
    </div>
</form>