<?php if (get_field('display_footer') != false) { ?>
    <?php if( get_field('display_footer','option') ): ?>
        <footer>
            <section class="footer" id="moth-footer" role="contentinfo" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
                <div class="footer-wrapper container">
                    <div class="row">


                        <?php 
                        $x = 0; 

                        $footer_column_layout = get_field('footer_column_layout','option');

                        if ($footer_column_layout == "1") {
                            $layout = "col-md-12";
                        } elseif ($footer_column_layout == "2") {
                            $layout = "col-md-6";
                        } elseif ($footer_column_layout == "3") {
                            $layout = "col-md-4";
                        } elseif ($footer_column_layout == "4") {
                            $layout = "col-md-3";
                        } else {
                            $layout = "col-md-2";
                        }

                        while($x <= $footer_column_layout) { ?> 

                            <?php if ( is_active_sidebar( 'footer_area_' . $x ) ) : ?>

                                <div id="ms-footer-area-<?php echo $x; ?>" class="<?php echo $layout; ?> widget-area" role="complementary">
                                    <?php dynamic_sidebar( 'footer_area_' . $x ); ?>
                                </div>
                                 
                            <?php endif; ?>

                        <?php $x++; } ?>

                    </div>
                    <!--/.row-->
                </div>
                <!--/.container-->
            </section>
            <!--/.footer-->

            <?php if( get_field('sub_footer','option') ): ?>
                
                <section id="sub-footer">
                    <div class="container sub-footer-wrapper">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mk-footer-copyright">
                                    <p>
                                      <?php 
                                        if(get_field('sub_footer_copyright_text','option')) {
                                            echo get_field('sub_footer_copyright_text','option');
                                        } else {
                                            echo "Copyright © " . date("Y") . ", " . get_bloginfo( 'name' ) . ". All rights reserved";
                                        } ?>
                                    </p>
                                </div>
                            </div>
                            <?php if( get_field('sub_footer_navigation','option') ): ?>
                                
                                <div class="col-md-6">
                                    <?php wp_nav_menu( 
                                        array( 
                                            'depth' => 1, 
                                            'theme_location' => 'sub-footer-nav', 
                                            'container_class' => 'sub-footer-navigation pull-right' 
                                            ) 
                                        ); 
                                    ?>
                                </div>
                                
                            <?php endif; ?>
                        </div>


                    </div>
                </section>
                
            <?php endif; ?>
            <!--/.footer-bottom-->
        </footer>
    <?php endif; ?>
<?php } // END IF SHOW FOOTER? ?>

<?php wp_footer(); ?>
  
</body>
</html>