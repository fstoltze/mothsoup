<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php do_action('theme_after_body_tag_start'); ?>

<?php if (get_field('display_header') != false) { ?>

  <header id="moth-header-1" class="moth-header header-style-1 header-align-left toolbar-true menu-hover-5 sticky-style-fixed moth-background-stretch boxed-header" role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
    <div class="moth-header-holder">
      <?php if (get_field('display_header_toolbar','option') != false) { get_template_part( 'views/header/toolbar' ); } ?>
      <div class="moth-header-inner add-header-height">
        <?php get_template_part( 'views/header/header' ); ?>
      </div>
    </div>
  </header>

<?php } // END IF DISPLAY HEADER? ?>

<?php do_action('theme_after_header'); ?>

  <?php get_template_part( 'views/layout/title' ); ?>


