<?php
get_header();
global $wp_query;
?>

<div id="ms-content-area" class="content-area container">
    <main id="main" class="site-main" role="main">

      <h1 class="search-title"> <?php echo $wp_query->found_posts; ?> <?php _e( 'search results found for', 'mothsoup' ); ?>: "<?php the_search_query(); ?>" </h1>

        <?php if ( have_posts() ) { ?>

            <div class="row">

            <?php while ( have_posts() ) { the_post(); ?>

			<div class="col-sm-12">
				<h3>
					<a href="<?php echo get_permalink(); ?>">
						<?php the_title();  ?>
					</a>
				</h3>
				<?php echo substr(get_the_excerpt(), 0,200); ?>
				<div class="h-readmore">
					<a href="<?php the_permalink(); ?>">
						<?php _e( 'Read More', 'mothsoup' ); ?>
					</a>
				</div>
			</div>

            <?php } ?>
 
            </div>

           <?php echo paginate_links(); ?>

        <?php } ?>

	</div>
</div>

<?php get_footer(); ?>