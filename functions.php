<?php 

/**
 * Enqueue Scripts and Styles for Front-End
 */
if ( ! function_exists( 'mothsoup_assets' ) ) :
function mothsoup_assets() {
	if (!is_admin()) {

		// Load JavaScripts
		wp_enqueue_script( 'jquery', get_bloginfo('stylesheet_directory') . '/ms-core/assets/js/jquery-3.2.1.slim.min.js', null, '3.2.1', true );
		wp_enqueue_script( 'popper', get_bloginfo('stylesheet_directory') . '/ms-core/assets/js/popper.min.js', null, '1.11.0', true );
        wp_enqueue_script( 'bootstrap', get_bloginfo('stylesheet_directory') . '/ms-core/assets/plugins/bootstrap/js/bootstrap.min.js', null, '4.0.0', true );
        wp_enqueue_script( 'font-awesome', get_bloginfo('stylesheet_directory') . '/ms-core/assets/plugins/font-awesome/js/fontawesome-all.min.js', null, '5.0.6', true );
        wp_enqueue_script( 'matchHeight', get_bloginfo('stylesheet_directory') . '/ms-core/assets/plugins/matchHeight/jquery.matchHeight-min.js', null, '0.7.2', true );
        wp_enqueue_script( 'mothsoup-app', get_bloginfo('stylesheet_directory') . '/ms-core/js/app-min.js', null, '1.0.0', true );
		//
		// Load Stylesheets
		wp_enqueue_style( 'static-style', get_template_directory_uri().'/ms-core/css/static-style-min.css', false, false );
		//wp_enqueue_style( 'dynamic-style', get_template_directory_uri().'/ms-core/dynamic-styles/global/dynamic-style.php', false, false );
		//wp_enqueue_style( 'slick', get_bloginfo('stylesheet_directory').'/inc/slick/slick.css' );
		//wp_enqueue_style( 'slick', get_template_directory_uri().'/core/slick/slick.css' );
		//wp_enqueue_style( 'custom', get_template_directory_uri().'/css/style.css' );
		// 
		// Load Google Fonts API
		//wp_enqueue_style( 'google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300' );
	
	}
}
add_action( 'wp_enqueue_scripts', 'mothsoup_assets' );
endif;
 
// THEME FUNCTIONS
require_once get_template_directory() . '/ms-core/framework/functions/fix.php'; // General WP fixes
require_once get_template_directory() . '/ms-core/framework/functions/navigation.php'; // Generates the widget areas of the theme
require_once get_template_directory() . '/ms-core/framework/functions/widget-areas.php'; // Generates the widget areas of the theme
require_once get_template_directory() . '/ms-core/framework/functions/api.php'; // Integrates the APIs into the theme
require_once get_template_directory() . '/ms-core/framework/bs4navwalker.php'; // Generates and supports main navigation
require_once get_template_directory() . '/ms-core/framework/functions/theme-functions.php'; // Controls the main functions of the theme
require_once get_template_directory() . '/ms-core/framework/functions/gf-fix.php'; // General Gravity Form fix
require_once get_template_directory() . '/ms-core/framework/functions/vc-fix.php'; // General Visual Composer fix
require_once get_template_directory() . '/ms-core/framework/functions/acf-include.php'; // Include Advanced Custom Fields in the theme
require_once get_template_directory() . '/ms-core/framework/functions/social.php'; // Handles the social media icons


// ADD IMAGE SIZES
add_image_size( '12-col', 1800, 940, true );
add_image_size( '9-col', 1237, 646, true );
add_image_size( '6-col', 810, 423, true );
add_image_size( '4-col', 525, 274, true );
add_image_size( '3-col', 382, 200, true );
add_image_size( '2-col', 240, 125, true );

// Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_false');


// ACF OPTIONS
get_template_part( 'ms-core/framework/admin/acf/acf', 'options' ); // Terminates the Option Page(s)
get_template_part( 'ms-core/framework/admin/acf/acf', 'export' ); // Generates the fields

get_template_part( 'ms-core/framework/acf/numslider/acf', 'numslider' ); // PLUGIN - Integrates a numberslider 



