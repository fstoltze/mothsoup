<?php get_header(); ?>

<div id="ms-content-area" class="content-area container">
    <main id="main" class="site-main" role="main">
      <?php
        if ( have_posts() ) :
        /* Start the Loop */
        while ( have_posts() ) : the_post(); ?>
        <div class="entry-content">
          <?php

            the_content( sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'mothsoup' ), get_the_title() ) );

            wp_link_pages( 
              array(
                'before'      => '<div class="page-links">' . __( 'Pages:', 'mothsoup' ),
                'after'       => '</div>',
                'link_before' => '<span class="page-number">',
                'link_after'  => '</span>',
              ) 
            );
          ?>
        </div><!-- .entry-content -->
      <?php endwhile; else : endif; ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>