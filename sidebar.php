<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package understrap
 */

if ( ! is_active_sidebar( 'page-sidebar' ) ) {
	return;
}
?>

<div class="widget-area <?php columnWidthSidebar(get_field('page_layout')); ?>" id="secondary" role="complementary">

	<?php dynamic_sidebar( 'page-sidebar' ); ?>

</div><!-- #secondary -->
