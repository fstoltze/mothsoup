<?php 

if( function_exists('acf_add_options_page') ) {
 
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Moth options',
		'menu_title' 	=> 'Moth options',
		'menu_slug' 	=> 'theme-options',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Menus & Sidebars',
	// 	'menu_title'	=> '» Menus & Sidebars',
	// 	'parent_slug'	=> 'theme-options',
	// ));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Typography',
	// 	'menu_title'	=> '» Typography',
	// 	'parent_slug'	=> 'theme-options',
	// ));

	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Styling',
	// 	'menu_title'	=> '» Styling',
	// 	'parent_slug'	=> 'theme-options',
	// ));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Plugins',
		'menu_title'	=> '» Theme Plugins',
		'parent_slug'	=> 'theme-options',
	));
 
}