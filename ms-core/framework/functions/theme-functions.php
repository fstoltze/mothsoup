<?php

function columnWidthSidebar($part = null) {
	$page_layout = get_field('page_layout');

    if ($part == "main" || $part == false) {
    	if ($page_layout != "full") { 
    		echo "col-sm-9"; 
    	} else {
    		echo "col-sm-12";
    	}
    }

    if ($part == "right" || $part == "left") {
    	if ($page_layout == "right") {
    		echo "col-sm-3 order-last";
    	} elseif ($page_layout == "left") {
    		echo "col-sm-3 order-first";
    	}
    }

}

function pageSidebar() {

	$page_layout = get_field('page_layout');

	if ($page_layout != "full" && is_page()) {
		get_sidebar( 'page-sidebar' );
	}

}

function alignTitle() {
	$title_align = get_field('page_title_align');
	if ($title_align == "center") {
		echo "text-center";
	} elseif ($title_align == "right") {
		echo "text-right";
	} else {}
}