<?php

// Register navigations and menus
function register_standard_menus() {
  register_nav_menu('primary-menu','Primary Menu');
  if(get_field('sub_footer_navigation','option')) {register_nav_menu('sub-footer-nav','Sub footer navigation');}
}
add_action( 'init', 'register_standard_menus' );