<?php

add_action('wp_head', 'head_add_api');
function head_add_api() { 

$google_analytics_id = get_field('google_analytics_id','option');
$google_tag_manager_id = get_field('google_tag_manager_id','option'); 

if( $google_tag_manager_id ): ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?php echo $google_tag_manager_id; ?>');</script>
<!-- End Google Tag Manager -->
<?php endif; 

if( $google_analytics_id ): ?>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google_analytics_id; ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', '<?php echo $google_analytics_id; ?>');
</script>
<!-- Google Analytics Tag End--> 
<?php endif; } 

add_action('theme_after_body_tag_start', 'body_add_api');
function body_add_api() { 

$google_analytics_id = get_field('google_analytics_id','option');
$google_tag_manager_id = get_field('google_tag_manager_id','option');

if( $google_tag_manager_id ): ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $google_tag_manager_id; ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif; }

?>