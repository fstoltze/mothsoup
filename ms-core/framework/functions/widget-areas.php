<?php

function mothsoup_widgets_functions() {
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'mothsoup' ),
        'id'            => 'footer_area_1',
        'description'   => __( 'Widgets in this area will be shown in footer area 1', 'mothsoup' ),
        'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 2', 'mothsoup' ),
        'id'            => 'footer_area_2',
        'description'   => __( 'Widgets in this area will be shown in footer area 2', 'mothsoup' ),
        'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 3', 'mothsoup' ),
        'id'            => 'footer_area_3',
        'description'   => __( 'Widgets in this area will be shown in footer area 3', 'mothsoup' ),
        'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 4', 'mothsoup' ),
        'id'            => 'footer_area_4',
        'description'   => __( 'Widgets in this area will be shown in footer area 4', 'mothsoup' ),
        'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Page Sidebar', 'mothsoup' ),
        'id'            => 'page-sidebar',
        'description'   => __( 'Widgets in this area will be shown in the sidebar of pages', 'mothsoup' ),
        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Post Sidebar', 'mothsoup' ),
        'id'            => 'post-sidebar',
        'description'   => __( 'Widgets in this area will be shown in the sidebar of posts', 'mothsoup' ),
        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widgettitle">',
        'after_title'   => '</h3>',
    ) );






}
add_action( 'widgets_init', 'mothsoup_widgets_functions' );