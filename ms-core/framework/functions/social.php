<?php

function FontAwesomeIcon($site = null,$style = null) {

    switch ($site) {

	    case "amazon":
	        return '<i class="fab fa-amazon"></i>';
	    break;

	    case "apple":
	        return '<i class="fab fa-apple"></i>';
	    break;

	    case "behance":
	        return '<i class="fab fa-behance"></i>';
	    break;

	    case "blogger":
	        return '<i class="fab fa-blogger-b"></i>';
	    break;

	    case "delicious":
	        return '<i class="fab fa-delicious"></i>';
	    break;

	    case "deviantart":
	        return '<i class="fab fa-deviantart"></i>';
	    break;
	    
	    case "digg":
	    return '<i class="fab fa-digg"></i>';
	    break;
	    
	    case "dribbble":
	    return '<i class="fab fa-dribbble"></i>';
	    break;
	    
	    case "dropbox":
	    return '<i class="fab fa-dropbox"></i>';
	    break;
	    
	    case "facebook":
	    return '<i class="fab fa-facebook-f"></i>';
	    break;
	    
	    case "flickr":
	    return '<i class="fab fa-flickr"></i>';
	    break;
	    
	    case "github":
	    return '<i class="fab fa-github"></i>';
	    break;
	    
	    case "google":
	    return '<i class="fab fa-google"></i>';
	    break;
	    
	    case "googleplus":
	    return '<i class="fab fa-google-plus-g"></i>';
	    break;
	    
	    case "lastfm": 
	    return '<i class="fab fa-lastfm"></i>';
	    break;
	    
	    case "linkedin": 
	    return '<i class="fab fa-linkedin-in"></i>';
	    break;
	    
	    case "instagram": 
	    return '<i class="fab fa-instagram"></i>';
	    break;
	    
	    case "pinterest": 
	    return '<i class="fab fa-pinterest-p"></i>';
	    break;
	    
	    case "reddit": 
	    return '<i class="fab fa-reddit-alien"></i>';
	    break;
	    
	    case "rss":
	    return '<i class="fas fa-rss"></i>';
	    break;
	    
	    case "skype":
	    return '<i class="fab fa-skype"></i>';
	    break;
	    
	    case "stumbleupon":
	    return '<i class="fab fa-stumbleupon"></i>';
	    break;
	    
	    case "tumblr": 
	    return '<i class="fab fa-tumblr"></i>';
	    break;
	    
	    case "twitter":
	    return '<i class="fab fa-twitter"></i>';
	    break;
	    
	    case "vimeo": 
	    return '<i class="fab fa-vimeo-v"></i>';
	    break;
	    
	    case "wordpress":
	    return '<i class="fab fa-wordpress-simple"></i>';
	    break;
	    
	    case "yahoo": 
	    return '<i class="fab fa-yahoo"></i>';
	    break;
	    
	    case "yelp":
	    return '<i class="fab fa-yelp"></i>';
	    break;
	    
	    case "youtube":
	    return '<i class="fab fa-youtube"></i>';
	    break;
	    
	    case "xing":
	    return '<i class="fab fa-xing"></i>';
	    break;
	    
	    case "renren":
	    return '<i class="fab fa-renren"></i>';
	    break;
	    
	    case "vk":
	    return '<i class="fab fa-vk"></i>';
	    break;
	    
	    case "weibo":
	    return '<i class="fab fa-weibo"></i>';
	    break;
	    
	    case "whatsapp":
	    return '<i class="fab fa-whatsapp"></i>';
	    break;
	    
	    case "soundcloud":
	    return '<i class="fab fa-soundcloud"></i>';
	    break;

	    default:
        return '<i class="far fa-user"></i>';
    }
}

function socialIcons($style = null,$color = null,$form = null,$size = null) {

    if( have_rows('social_media_networks','option') ): ?>

    <?php

    if ($size != null) {
    	$size = "si-" . $size;
    } else {}

    ?>

        <ul class="social-icons">

        <?php while( have_rows('social_media_networks','option') ): the_row(); 

            $site = get_sub_field('social_site');
            $url = get_sub_field('social_url');

            ?>

            <li class="social-icon si-<?php echo $style; ?> si-<?php echo $form; ?> <?php echo $size; ?> si-<?php echo $color; ?>">

                <a href="<?php echo $url; ?>">
                    <?php echo FontAwesomeIcon($site); ?>
                </a>

            </li>

        <?php endwhile; ?>

        </ul>

    <?php endif; 

}